import { ColseButton } from '../ColseButton';
import { useState } from 'react';
import { FeedbackTypeStep } from '../WidegForm/Steps/FeedbackTypeStep';

import bugImageUrl from '../../assets/bug.svg';
import ideaImageUrl from '../../assets/idea.svg';
import thoughtImageUrl from '../../assets/thought.svg';

export const feedbackTypes = {
    Error: {
        title: 'Problemas',
        image: {
            source: bugImageUrl,
            alt: 'Imagem de um inseto'
        },
    },
    Iedia: {
        title: 'Ideias',
        image: {
            source: ideaImageUrl,
            alt: 'Imagem de uma lampada'
        },
    },
    Ordem: {
        title: 'Outros',
        image: {
            source: thoughtImageUrl,
            alt: 'Imagem de pensamento'
        },
    },
};

export type FeedBackType = keyof typeof feedbackTypes;

export function WidgetForm() {

    const [feedbackType, setFeedbackType] = useState<FeedBackType | null>(null)

    return(
        <div className="bg-zinc-900 p-4 relative rounded-2xl mb-4 flex flex-col items-center shadow-lg w-[calc(100vw-2rem)] md:w-auto">
            <header>
                <span className="text-xl leading-6">Deixe seu feedback</span>
                <ColseButton />
            </header>

            {!feedbackType ? (
                <FeedbackTypeStep onFeedbackTypeChanged={setFeedbackType}/>

            ) : (
                <p>Hello World</p>
            )}

            <footer className="text-xs text-neutal-400">
                Feito com amor para voce
            </footer>
        </div>
    );
}
